<?php

class nhtsaClient
{
    /**
     * @var \GuzzleHttp\Client|null
     */
    private $client = null;
    private const NOT_RATED = 'Not Rated';

    function __construct()
    {
        $this->client = new GuzzleHttp\Client([
            'base_uri'  => 'https://one.nhtsa.gov/webapi/api/SafetyRatings/',
            'protocols' => ['https'],
            'timeout'   => 10
        ]);
    }

    /**
     * @param array $params
     * @return mixed
     * example call https://one.nhtsa.gov/webapi/api/SafetyRatings/modelyear/2013/make/Acura/model/rdx?format=json
     */
    public function getVehicles(array $params) :array
    {
        $params = array_change_key_case($params, CASE_LOWER);

        //filter important values
        $options = [
            'modelyear'    => (string)$params['modelyear'],
            'manufacturer' => (string)$params['manufacturer'],
            'model'        => (string)$params['model']
        ];

        $response = $this->client->request(
            'GET',
            'modelyear/' . $options['modelyear'] . '/make/' . $options['manufacturer'] . '/model/' . $options['model'],
            ['format' => 'json']
        );

        $body = json_decode((string)$response->getBody());

        $result = [
            'Count'   => $body->Count,
            'Results' => $body->Results
        ];

        if (!$result['Count'] || (isset($params['withrating']) && !$params['withrating'])) {
            return $result;
        }

        foreach ($result['Results'] as &$row) {
            if (!isset($row->VehicleId)) {
                $row->CrashRating = self::NOT_RATED;
                continue;
            }

            $row->CrashRating = $this->getCrashRating($row->VehicleId);
        }

        return $result;
    }

    /**
     * @param int $id
     * @return string
     */
    public function getCrashRating(int $id) :string
    {
        $response = $this->client->request(
            'GET',
            'VehicleId/' . $id,
            ['format' => 'json']
        );

        $body = json_decode((string)$response->getBody());

        //be save
        if (!count($body->Results) ) {
            return self::NOT_RATED;
        }

        return $body->Results[0]->OverallRating;
    }


}