<?php

use \Psr\Http\Message\ServerRequestInterface as Request,
    \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../app/nhtsaClient.php';

$c = new \Slim\Container();
$c['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        return $c['response']->withStatus(500)
            ->withHeader('Content-Type', 'text/html')
            ->write('Something went wrong!');
    };
};
$app = new \Slim\App($c);


/**
 * https://nhtsa-client.dev/vehicles/2013/Acura/rdx
 */
$app->get('/vehicles/{modelyear}/{manufacturer}/{model}', function (Request $request, Response $response, array $args) {
    $client = new nhtsaClient();

    $queryParams = $request->getQueryParams();
    $params = $args;
    $params['withrating'] = (isset($queryParams['withRating']) && $queryParams['withRating'] == 'true') ? true : false;


    $result = $client->getVehicles($params);
    $response->getBody()->write(json_encode($result));
});


$app->post('/vehicles', function (Request $request, Response $response) {

    $body = (string)$request->getBody();
    $body = json_decode($body, true);

    if (json_last_error() !== JSON_ERROR_NONE) {
        $newResponse = $response->withStatus(400);
        return $newResponse;
    }

    $client = new nhtsaClient();
    $result = $client->getVehicles($body);

    $response->getBody()->write(json_encode($result));
});

$app->run();